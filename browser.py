import argparse
import os
import re
import requests
from bs4 import BeautifulSoup
from colorama import Fore

# write your code here
parser = argparse.ArgumentParser()
parser.add_argument("dir")
args = parser.parse_args()

if not os.path.exists(args.dir):
    os.makedirs(args.dir)

internet_cache = {}

index_urls = {
    "bloomberg": "bloomberg.com",
    "nytimes": "nytimes.com"
}

history = []


def last_visited_page():
    try:
        result = internet_cache[history[-2]]
    except IndexError:
        result = None

    print(result)
    return result


class TextBrowser:

    commands = {
        "exit": lambda: exit(),
        "back": last_visited_page
    }

    def start(self):
        while True:
            input_text = input()

            if input_text not in self.commands:
                result = re.match(r"(\w+)\.(\w+)", input_text)
                if result or input_text in index_urls:

                    if input_text in index_urls:
                        input_text = index_urls.get(input_text)

                    # Lunch internet crawler
                    if not input_text.startswith("https"):
                        url = f"https://{input_text}"
                    else:
                        url = input_text
                        input_text = input_text[8:]

                    if input_text in internet_cache:
                        web_page = internet_cache.get(input_text)
                    else:
                        try:
                            user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
                            web_page = requests.get(
                                url, headers={
                                    "User-Agent": user_agent
                                }
                            )
                            web_page = BeautifulSoup(web_page.content, 'html.parser')
                            for i in web_page.find_all("a"):
                                i.string = "".join([Fore.BLUE, i.get_text(), Fore.RESET])

                            domain_l1 = input_text.split(".")[0]
                            web_page = web_page.get_text()
                            with open(f"{args.dir}/{domain_l1}", "w+", encoding="utf-8") as file_output:
                                file_output.write(web_page)
                        except requests.exceptions.ConnectionError:
                            web_page = None

                    if web_page:
                        result = web_page
                        print(result)
                        # domain_l1 = input_text.split(".")[0]
                        # with open(f"{args.dir}/{domain_l1}", "w+", encoding="utf-8") as file_output:
                        #     file_output.write(result)
                        history.append(input_text)
                        internet_cache[input_text] = result
                    else:
                        print("Error: Incorrect URL")
                else:
                    print("Error: Incorrect URL")
            else:
                # Execute commands

                executor = self.commands.get(input_text)
                executor()


TextBrowser().start()
